package taxipark

/*
 * Task #1. Find all the drivers who performed no trips.
 */
fun TaxiPark.findFakeDrivers(): Set<Driver> =
//        allDrivers.filter { driver -> driver !in trips.map { it.driver } }.toSet()

//        allDrivers.filter { driver -> trips.none { it.driver == driver} }.toSet()

//        allDrivers.minus(trips.map { it.driver })

        allDrivers - trips.map { it.driver }

/*
 * Task #2. Find all the clients who completed at least the given number of trips.
 */
fun TaxiPark.findFaithfulPassengers(minTrips: Int): Set<Passenger> =
        /*if (minTrips == 0) allPassengers else
            trips.flatMap { it.passengers }
                    .groupingBy { it }
                    .eachCount()
                    .filter { it.value >= minTrips }
                    .keys*/

        /*trips.flatMap(Trip::passengers)
                .groupBy { passenger -> passenger }
                .filterValues { group -> group.size >= minTrips }
                .keys*/

        allPassengers
                .filter { passenger ->
                    trips.count { passenger in it.passengers } >= minTrips
                }
                .toSet()

/*
 * Task #3. Find all the passengers, who were taken by a given driver more than once.
 */
fun TaxiPark.findFrequentPassengers(driver: Driver): Set<Passenger> =
        trips.filter { it.driver == driver }
                .flatMap { it.passengers }
                .groupingBy { it }
                .eachCount()
                .filter { it.value > 1 }
                .keys

/*
 * Task #4. Find the passengers who had a discount for majority of their trips.
 */
fun TaxiPark.findSmartPassengers(): Set<Passenger> {
    /*val (discountTrips, regularTrips) = trips.partition { it.discount != null }
    val discountPassengerTrips = discountTrips.flatMap { it.passengers }
            .groupingBy { it }
            .eachCount()
    val regularPassengerTrips = regularTrips.flatMap { it.passengers }
            .groupingBy { it }
            .eachCount()
    val smartPassengers = mutableSetOf<Passenger>()
    discountPassengerTrips.forEach {
        if (it.value > regularPassengerTrips.getOrDefault(it.key, 0)) {
            smartPassengers.add(it.key)
        }
    }
    return smartPassengers*/

    /*return allPassengers.filter { passenger ->
        val discountTrips = trips.count { trip -> passenger in trip.passengers && trip.discount != null }
        val regularTrips = trips.count { trip -> passenger in trip.passengers && trip.discount == null }
        discountTrips > regularTrips
    }.toSet()*/

    val (discountTrips, regularTrips) = trips.partition { it.discount != null }
    return allPassengers
            .filter { passenger ->
                discountTrips.count { trip -> passenger in trip.passengers } >
                        regularTrips.count { trip -> passenger in trip.passengers }
            }
            .toSet()
}

/*
 * Task #5. Find the most frequent trip duration among minute periods 0..9, 10..19, 20..29, and so on.
 * Return any period if many are the most frequent, return `null` if there're no trips.
 */
fun TaxiPark.findTheMostFrequentTripDurationPeriod(): IntRange? {
    val rangeCount = trips.groupingBy { it.duration.rangeByTen() }.eachCount()
    return rangeCount.maxBy { it.value }?.key
}

fun Int.rangeByTen(): IntRange {
    val begin = this - (this % 10)
    val end = begin + 9
    return begin..end
}

/*
 * Task #6.
 * Check whether 20% of the drivers contribute 80% of the income.
 */
fun TaxiPark.checkParetoPrinciple(): Boolean {
    val topPerformersIncome = trips.groupingBy(Trip::driver)
            .fold(0.0) { sum, trip -> sum + trip.cost }
            .toList()
            .sortedByDescending { (_, income) -> income }
            .take((allDrivers.size * 0.2).toInt())
            .sumByDouble { (_, income) -> income }

    val totalIncome = trips.sumByDouble(Trip::cost)
    return trips.isNotEmpty() && topPerformersIncome >= totalIncome * 0.8
}