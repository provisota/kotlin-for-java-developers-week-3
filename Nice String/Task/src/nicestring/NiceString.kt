package nicestring

fun String.isNice(): Boolean {
    return listOf(noBadStrings(this), hasThreeVowels(this), hasDoubles(this))
            .count { it } >= 2
}

/**
 * It doesn't contain substrings bu, ba or be;
 */
fun noBadStrings(string: String): Boolean {
    /*return if (string.contains("bu") ||
            string.contains("ba") ||
            string.contains("be")) 0 else 1*/
    return setOf("bu", "ba", "be").none { string.contains(it) }
}

/**
 * It contains at least three vowels (vowels are a, e, i, o and u);
 */
fun hasThreeVowels(string: String): Boolean {
//    val score: Int = string.count { it == 'a' || it == 'e' || it == 'i' || it == 'o' || it == 'u' }
    return string.count { it in "aeiou" } >= 3
}

/**
 * It contains a double letter (at least two similar letters following one another), like b in "abba".
 */
fun hasDoubles(string: String): Boolean {
//    val doublesCount = string.toList().zipWithNext().count { it.first == it.second }
    return string.zipWithNext().any { it.first == it.second }
}
